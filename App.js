import React, { Component } from 'react';
import { SafeAreaView, View, StyleSheet, Text } from 'react-native';
import { createAppContainer, createDrawerNavigator, DrawerItems } from 'react-navigation';

//CustomDrawer
import Header from './src/routes/Drawer/Header';
import Footer from './src/routes/Drawer/Footer';

//Screens
import Home from './src/routes/Home';
import Suporte from './src/routes/Suport';

const DrawerContent = (props) => (
  <SafeAreaView style={styles.container}>
    <View style={{ flex:1, justifyContent:'space-between' }}>

      <View style={styles.header}>
       <Header />
      </View>

      <View style={{ marginTop:-350 }}>
        <DrawerItems {...props} />
      </View>
      
      <View style={{  }}>
        <Footer />
      </View>

    </View>
  </SafeAreaView>
)

const DrawerNavigator = createDrawerNavigator({
  Home:{
    screen:Home
  },
  Suporte:{
    screen:Suporte
  }
}, {
  contentComponent:DrawerContent,
  defaultNavigationOptions:{

  }
});

const styles = StyleSheet.create({
  container:{
    flex:1
  },
  header:{
    height:150,
    alignItems:'center',
    justifyContent:'center',
  }
});

export default createAppContainer(DrawerNavigator);