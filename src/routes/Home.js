import React, { Component } from 'react';
import { createBottomTabNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome5';

//Screens
import Contato from './Contato';
import Suporte from './Suport';
import Teste from './Teste';

const TabNavigator = createBottomTabNavigator({
    Contato:{
        screen:Contato,
        navigationOptions: () => ({
            tabBarIcon: ({tintColor}) => (
                <Icon
                    name="home"
                    color={tintColor}
                    size={24}
                />
            )
        })
    },
    Suporte:{
        screen:Suporte,
        navigationOptions: () => ({
            tabBarIcon: ({tintColor}) => (
                <Icon
                    name="trash"
                    color={tintColor}
                    size={24}
                />
            )
        })
    },
    Teste:{
        screen:Teste,
        navigationOptions: () => ({
            tabBarIcon: ({tintColor}) => (
                <Icon
                    name="male"
                    color={tintColor}
                    size={24}
                />
            )
        })
    }
}, {
    swipeEnabled:true,
    tabBarOptions:{
        showLabel:false,
        showIcon:true
    }
})

export default TabNavigator;