import React, { Component } from 'react';
import { Text, StyleSheet, View, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

export default class Header extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image source={require('../../../assets/images/profile.jpg')} style={{ width:100, height:100, borderRadius:10 }} />
        <Text>Bem-vindo, Adonai</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
    container:{
        justifyContent:'center',
        alignItems:'center'
    }
});
