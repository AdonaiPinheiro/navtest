import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

export default class Footer extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Icon name='sign-in-alt' size={30} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container:{
    backgroundColor:'#CCC',
    height:50,
    justifyContent:'center',
    alignItems:'center'
  }
})
